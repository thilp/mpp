package MPP;

use 5.010_001;
use strict;
use warnings;
use open IO => 'utf8';
use open ':std';
use warnings::register;
use subs qw( warn error );

our $VERSION = "1.2.0";

use File::Spec;
use File::Temp;
use File::Find;
use File::Copy;
use File::Path qw(make_path remove_tree);
use File::Basename qw(fileparse);
use Cwd qw(cwd realpath);
use Safe;
use Storable qw(freeze thaw);
use IPC::Cmd qw(can_run run);

use Class::Struct Macro => { name => '$', body   => '$', arglist => '@' };
use Class::Struct Alias => { name => '$', target => '$', type    => '$' };

BEGIN {
    use parent 'Exporter';

    our @EXPORT = qw(mpp);

    our %EXPORT_TAGS;
    $EXPORT_TAGS{default} = [qw(mpp errors)];
    $EXPORT_TAGS{config} =
      [
        qw(accept_define accept_input_config accept_absolute_paths
           assistant accept_out_repository fact_path fact_timeout)
      ];
    $EXPORT_TAGS{std}     = [
        @{ $EXPORT_TAGS{default} },
        @{ $EXPORT_TAGS{config} },
        qw(reset_inclusion_tracking reset_mpp reset_mpp_hard list_includes)
    ];
    $EXPORT_TAGS{all} =
      [ @{ $EXPORT_TAGS{std} }, qw(escape separator) ];
    $EXPORT_TAGS{_dev} = [
        @{ $EXPORT_TAGS{all} },
        qw(config_from_input rebuild_regexes
          tokenize process_tokens
          flush define_user_macro extract_args)
    ];

    Exporter::export_ok_tags($_) for keys %EXPORT_TAGS;
}

# it is legit to use MPP from the outside of a project repository
my $ACCEPT_OUT_REPOSITORY;
my $ACCEPT_DEFINE;            # 'define' constructs are legit
my $ACCEPT_INPUT_CONFIG;      # 'mpp_XXX: YYY' constructs are legit
my $ACCEPT_ABSOLUTE_PATHS;    # 'include(/an/absolute/path)' is legit

my ( $FACT_PATH, $FACT_TIMEOUT );

my $ERRORS;                   # error count

my $ESCAPED;         # true iff the previous token was an escape sequence
my $INCLUDES;        # table of included filenames from parent files
my $INCLUDES_ALL;    # like $INCLUDES, but does not forget anything
my $ESC;             # escape sequence
my $SEP;             # separator sequence
my %QUOTING;
my %GROUPING;

my $SAFE;
my $REPOSITORY_ROOT;    # absolute path to the root of the current repository
my $CURRENT_DIR;

my $TOKEN_STREAM;    # the stream of produced tokens before they are executed
my $ASSISTANT;

my $DEFAULT_MACROS;  # table of always-defined macros
my $USER_MACROS;     # table of user-defined macros
my $USER_ALIASES;    # table of aliased macros

my (             # regexes; a leading 'c' indicates captures
    $rESC, $rSEP,    # regex-escaped equivalents for $ESC and $SEP
    $rWORD,                # a token that can be an identifier
    $rKEYWORD,             # an escaped WORD, and thus an identifier
    $rcKEYWORD,            # capture for KEYWORD
    $rcPARAM,              # matches a formal argument (a Perl scalar variable)
    $CLOSE_MACRO,          # final token of a macro definition's body
    $rCLOSE_MACRO,         # match the end of a macro definition's body
    $rCLOSE_MACRO_FAIL,    # try to match a failed CLOSE_MACRO
);

sub reset_mpp {
    $ESCAPED      = 0;
    $INCLUDES     = {};
    $INCLUDES_ALL = {};
    $TOKEN_STREAM = [];
    $ESC          = q{%%};
    $SEP          = q{,};
    $CLOSE_MACRO  = q{=end};
    $USER_MACROS  = {};
    $USER_ALIASES = {};
    $CURRENT_DIR  = get_current_dir();
    return rebuild_regexes();
}

sub reset_mpp_hard {
    $ACCEPT_OUT_REPOSITORY = 1;
    $ACCEPT_DEFINE         = 1;
    $ACCEPT_INPUT_CONFIG   = 1;
    $ACCEPT_ABSOLUTE_PATHS = 0;
    $ERRORS                = 0;
    $ASSISTANT             = 0;
    $FACT_PATH             = undef;
    $FACT_TIMEOUT          = 10;
    return reset_mpp();
}

sub rebuild_regexes {
    $rESC              = qr/(?x-i: \Q$ESC\E )/;
    $rSEP              = qr/(?x-i: \Q$SEP\E )/;
    $rWORD             = qr/(?xi: [a-z_][a-z0-9_-]* (?<!-) )/;
    $rKEYWORD          = qr/(?x: $rESC $rWORD )/;
    $rcKEYWORD         = qr/(?x: $rESC ( $rWORD ) )/;
    $rcPARAM           = qr/(?xi: \$ ( [a-z_][a-z0-9_]* ) )/;
    $rCLOSE_MACRO      = qr/(?: ^ \h* \Q$CLOSE_MACRO\E \s* (?:\n|\z) )/ixm;
    $rCLOSE_MACRO_FAIL = qr/(?: \s = \s* end \b )/ixm;
    return 1;
}

# Returns the path to the repository root relative to the current directory.
# The repository root is characterized by containing the current directory and
# having a readable "ref" subdirectory.
# This function returns UNDEF if we are not under a valid repository, or the
# absolute path to the repository root otherwise.
sub get_repository_root {
    my $current = cwd();
    my $root    = $current;
    $root = realpath( File::Spec->catdir( $root, File::Spec->updir() ) )
      while $root ne File::Spec->rootdir()
      && !-d File::Spec->catdir( $root, 'ref' );
    return if $root eq File::Spec->rootdir();
    return $root;
}

sub get_current_dir { realpath( File::Spec->curdir() ) }

# default values
BEGIN {
    reset_mpp_hard();

    %QUOTING = (
        q{'} => q{'},
        q{"} => q{"},
    );
    %GROUPING = (
        q{(} => q{)},
        q{[} => q{]},
        q[{] => q[}],
    );

    $SAFE = new Safe;

    sub define_user_macro {
        my ( $name, $arglist, $body ) = @_;
        my $lname = lc $name;
        eval {
            die qq{can't define a \$STACK variable (in "$name"'s prototype)\n}
              if grep { $_ eq '$STACK' } @$arglist;
            die qq{can't define a macro named like "define"\n}
              if $lname eq 'define';

            warn qq{defining "$name" overrides a builtin}
              if exists $DEFAULT_MACROS->{$lname};
            warn qq{only the last definition of "$name" will be used}
              if exists $USER_MACROS->{$lname};

            $USER_MACROS->{$lname} = Macro->new(
                name    => $name,
                arglist => $arglist,
                body    => $body
            );

        };    # eval
        if ($@) {
            chomp $@;
            die qq{invalid macro definition for "$name": $@ [removed]};
        }
        return;
    }

    sub include {
        my ( $restore_context, $args ) = @_;
        no warnings 'once';
        local $Storable::Deparse = 1;
        local $Storable::Eval    = 1;
        use warnings;
        my %save_env = (
            TOKEN_STREAM => freeze($TOKEN_STREAM),
            USER_MACROS  => freeze($USER_MACROS),
            ESC          => $ESC,
            SEP          => $SEP,
            INCLUDES     => freeze($INCLUDES),
            USER_ALIASES => freeze($USER_ALIASES),
            CURRENT_DIR  => $CURRENT_DIR,
        );
        my $filename = File::Spec->canonpath( $args->[0] );
        die qq{"$filename" is an absolute path and thus rejected\n}
          if !$ACCEPT_ABSOLUTE_PATHS
          && File::Spec->file_name_is_absolute($filename);
        $INCLUDES_ALL->{$filename} += 1;
        $filename = File::Spec->catpath( $CURRENT_DIR, $filename );

        if ( exists $INCLUDES->{$filename} ) {
            die qq{file "$filename" already included; to avoid }
              . qq{infinite recursion, such further includes are removed\n};
        }
        else { $INCLUDES->{$filename} = 1 }
        my $text = do {
            open my $fh, '<', $filename
              or die qq{unable to read "$filename": $!\n};
            local $/;
            <$fh>;
        };
        $text         = mpp( $text, $filename );
        $TOKEN_STREAM = thaw( $save_env{TOKEN_STREAM} );
        $INCLUDES     = thaw( $save_env{INCLUDES} );
        $CURRENT_DIR  = $save_env{CURRENT_DIR};
        if ($restore_context) {
            $USER_MACROS  = thaw( $save_env{USER_MACROS} );
            $USER_ALIASES = thaw( $save_env{USER_ALIASES} );
            ( $ESC, $SEP ) = @save_env{qw(ESC SEP)};
        }
        rebuild_regexes();
        chomp $text
          if @$TOKEN_STREAM
          && $TOKEN_STREAM->[0]{type} eq 'std';
        $text =~ s/ \A \s+ $ | ^ \s+ \z //xgm;
        return $text;
    }

    $DEFAULT_MACROS = {

        'assistant-begin' => sub {
            return if $ASSISTANT;
            shift @$TOKEN_STREAM
              while @$TOKEN_STREAM
              and ( $TOKEN_STREAM->[0]{type} ne 'macro'
              || $TOKEN_STREAM->[0]{name} ne 'assistant-end');
            shift @$TOKEN_STREAM if @$TOKEN_STREAM;
            return;
        },

        'assistant-end' => sub {
            return if $ASSISTANT;
            die q{don't use "assistant-end" before a corresponding }
              . qq{"assistant-begin"!\n};
        },

        'date' => sub { scalar localtime() },

        'cpp-include' => sub { unshift @_, 0; goto &include },

        'scoped-include' => sub { unshift @_, 1; goto &include },

        define => sub {
            my $args = shift;
            @_ = @$args;
            goto &define_user_macro;
        },

        'alias' => sub {
            my $args = shift;
            my ( $alias_full, $target_full ) = @$args;
            my ( $alias_lc, $target_lc ) = map { lc } @$args;

            die "alias and target name must be different; "
              . " remainder: names are case-insensitive\n"
              if $alias_lc eq $target_lc;

            my $alias = Alias->new( name => $alias_full, target => $target_lc );

            $target_lc = $USER_ALIASES->{$target_lc}->target
              while exists $USER_ALIASES->{$target_lc};
            if ( exists $USER_MACROS->{$target_lc} ) {
                $alias->type('user');
            }
            elsif ( exists $DEFAULT_MACROS->{$target_lc} ) {
                $alias->type('default');
            }
            else {
                die qq{can't create alias: unknown macro "$target_full"\n};
            }

            if ( exists $USER_ALIASES->{$alias_lc} ) {
                my $tmp = $USER_ALIASES->{$alias_lc}->target;
                $tmp = $USER_MACROS->{$tmp}->name if $alias->type eq 'user';
                warn qq{alias "$alias_full" retargeted from "$tmp" }
                  . qq{to "$target_full"};
            }

            warn qq{new alias "$alias_full" will mask all macros }
              . q{of the same name}
              if exists $USER_MACROS->{$alias_lc}
              || exists $DEFAULT_MACROS->{$alias_lc};

            $USER_ALIASES->{$alias_lc} = $alias;

            return;
        },

        'display-code' => sub {
            my ( $filename, $lang, $linespecs ) = @{ shift() };

            unless ( defined $lang && $lang ne '' ) {
                my ($name, $path, $suffix) = fileparse( $filename, qr/ (?<=\.) [^.]* /x );
                for ( $suffix ) {
                    $lang = 'c' if /^(?:c|h)$/i;
                    $lang = 'cpp' if /^(?:cc|hh|cpp|hpp|c\+\+|h\+\+|hxx)$/i;
                    $lang = 'java' if /^(?:java|class)$/i;
                    $lang = 'bash' if /^sh$/;
                    $lang = 'perl' if /^(?:pl|pm|t)$/;
                    $lang = 'python' if /^py$/;
                    $lang = 'xml' if /^xml$/;
                }
            }

            $filename = File::Spec->canonpath($filename);
            unless ($ACCEPT_ABSOLUTE_PATHS) {
                die "paths must be relative to the $REPOSITORY_ROOT/ref/ "
                  . "directory\n"
                  if File::Spec->file_name_is_absolute($filename);
            }
            unless ( File::Spec->file_name_is_absolute($filename) ) {
                die "can't find REPOSITORY_ROOT"
                  unless defined $REPOSITORY_ROOT;
                $filename =
                  File::Spec->catfile( $REPOSITORY_ROOT, 'ref',
                    File::Spec->splitpath($filename) );
            }
            open my $fh, '<', $filename or die "$filename: $!\n";
            local $/ = "\n";
            my @lines = <$fh>;
            $linespecs = [ "1-" . @lines ] unless defined $linespecs;

            $lang = defined $lang && $lang ne '' ? ".$lang" : '';
            local $" = '';
            my @blocks;
            for (@$linespecs) {
                my ( $start, $stop ) = m/ ^ \h* (\d*) \h* - \h* (\d*) \h* $ /x;
                die qq{invalid linespec "$_": give at least one number\n}
                  unless $start ne '' || $stop ne '';
                die qq{invalid linespec "$_": $start > $stop\n}
                  if $start ne '' && $stop ne '' && $start > $stop;
                $start = 1      if $start eq '' || $start < 1;
                $stop  = @lines if $stop  eq '' || $stop  > @lines;

                my @tmp = @lines[ ( $start - 1 ) .. ( $stop - 1 ) ];
                push @blocks,
                  (   ( '~' x 10 )
                    . qq< { $lang .numberLines startFrom="$start" }\n>
                      . "@tmp"
                      . ( '~' x 30 )
                      . "\n" );
            }
            local $" = "\n";
            my $out = "\n<!---->\n\n@blocks\n<!---->\n";
            chomp $out;
            return $out;
        },

        'example-gen' => sub {
            die "can't find REPOSITORY_ROOT" unless defined $REPOSITORY_ROOT;
            my ( $cmdlist, $path ) = @{ shift() };

            # Code for FORWARD COMPATIBILITY
            # We want to change argument order, but we must not break existing
            # code. That's why this warning will inform users of the old
            # syntax about the future removal of it.
            if ( ref $cmdlist ne 'ARRAY' && ref $path eq 'ARRAY' ) {
                warn "DEPRECATED argument order for ${ESC}example-gen:\n"
                . "\talthough the PATH,CMDLIST order is still supported, "
                  . "it will soon be supplanted by the (more consistent) "
                  . "CMDLIST[,PATH] order.\n"
                  . "\tPLEASE UPGRADE YOUR CODE ACCORDINGLY AS SOON "
                  . "AS POSSIBLE\n"
                  . "\t(since v0.1.4)\n";

                ($cmdlist, $path) = ($path, $cmdlist);
            }

            my $outdir = File::Temp->newdir();
            my $outfile = File::Spec->catfile( $outdir, 'out.txt' );

            if ( defined $path ) {
                my $inpath =
                  File::Spec->catdir( $REPOSITORY_ROOT, 'ref', $path );
                make_with_fact( $inpath, $outfile, $cmdlist )
                  or die "execution failed";
            }
            else {
                my $cmds = do {
                    my @cmds =
                      map { ( my $a = $_ ) =~ s/^(.*)#\h*hide\b.*$/\@$1/; $a }
                      @$cmdlist;
                    join ";\n" => @cmds;
                };
                fact( 'print', 'mppsmpgen', '--output', $outfile, $cmds );
            }

            open my $fh, '<', $outfile
              or die "cannot open output file $outfile: $!";
            my @lines;
            local $/ = "\n";
            push @lines, ( ' ' x 4 ) . $_ while $_ = <$fh>;
            chomp( my $r = join '' => @lines );
            return $r;
          },

        'image' => sub {
            my ( $path, $widthratio, $caption ) = @{ shift() };

            if ( defined $widthratio && $widthratio ) {
                warn q{the width ratio you gave to the "image" macro }
                  . qq{("$widthratio") seems incorrect\n}
                  if $widthratio !~ / ^ [0-9]* (?: \. [0-9]+ )? $ /x;
                $widthratio = "[width=$widthratio\\textwidth]";
            }
            else { $widthratio = '[width=\\textwidth]' }

            $caption = defined $caption ? "\\\\\n\\emph{$caption}\n" : "\n";

            return
                "\\begin{center}\n"
              . "\\includegraphics${widthratio}\{$path}"
              . $caption
              . "\\end{center}";
          },
    };
}

sub warn { warnings::warnif("MPP: warning: @_") }
sub error { $ERRORS += 1; warnings::warnif("MPP: error: @_") }
sub errors { $ERRORS }

sub accept_out_repository {
    $ACCEPT_OUT_REPOSITORY = shift() ? 1 : 0;
    unless ($ACCEPT_OUT_REPOSITORY) {
        warn 'deactivating ACCEPT_ABSOLUTE_PATHS along with '
          . 'ACCEPT_OUT_REPOSITORY'
          if $ACCEPT_ABSOLUTE_PATHS;
        $ACCEPT_ABSOLUTE_PATHS = 0;
    }
    $ACCEPT_OUT_REPOSITORY;
}
sub accept_define         { $ACCEPT_DEFINE         = shift() ? 1 : 0 }
sub accept_input_config   { $ACCEPT_INPUT_CONFIG   = shift() ? 1 : 0 }
sub accept_absolute_paths { $ACCEPT_ABSOLUTE_PATHS = shift() ? 1 : 0 }

sub assistant { $ASSISTANT = shift() ? 1 : 0 }

sub fact_timeout { $FACT_TIMEOUT = shift if @_; $FACT_TIMEOUT }

sub fact_path {
    $FACT_PATH = realpath( shift() ) if @_;
    $FACT_PATH;
}

sub escape {
    $ESC = shift if @_;
    warn "ESCAPE character is likely to cause lexing errors"
      if grep { $_ eq $ESC } keys(%QUOTING), values(%QUOTING), $SEP;
    $ESC;
}

sub separator {
    $SEP = shift if @_;
    warn "SEPARATOR character is likely to cause lexing errors"
      if grep { $_ eq $SEP } keys(%QUOTING), values(%QUOTING), $ESC;
    $SEP;
}

sub fact {
    can_run('mono') or die "mono is not installed\n";
    defined $FACT_PATH
      or die "FACT_PATH is not set: see the fact_path function\n";
    my $buff = '';
    my ( $ok, $err ) = run(
        command => [ 'mono', $FACT_PATH, @_ ],
        buffer  => \$buff,
        timeout => $FACT_TIMEOUT
    );
    $ok ? $buff : die $err."\nFact precisely said:\n$buff";
}

# will die if fact() dies
sub make_with_fact {
    my ( $inpath, $outfile, $cmdlist ) = @_;
    my $cmds = do {
        my @cmds =
          map { ( my $a = $_ ) =~ s/^(.*)#\h*hide.*$/\@$1/; $a } @$cmdlist;
        join ";\n" => @cmds;
    };
    my $outdir = do { my @tmp = fileparse($outfile); $tmp[1] };
    my $pkgin  = File::Spec->catfile( $outdir, 'mpp.ff' );
    my $pkgout = File::Spec->catfile( $outdir, 'mpp.ff' );

    eval { fact( 'package', 'create', $inpath, $pkgin ) };
    die "Fact can't create a package: $@" if $@;

    eval { fact( 'make', 'make', $pkgin, $pkgout ) };
    die "Fact can't make: $@" if $@;

    eval {
        fact( 'print', 'mppsmpgen', '--output', $outfile,
            "\@import $pkgout; \@fact package extract $pkgout .; $cmds" );
    };
    die "Fact can't execute mppsmpgen: $@\n" if $@;
    return 1;
}

# NOT YET IMPLEMENTED!
sub make_without_fact {
    my ( $inpath, $cmdlist ) = @_;
    my @cmds = map { my $hide = ( my $cmd = $_ ) =~ s/#.+$//s; [ $cmd, $hide ] }
      @$cmdlist;
    my @output;
    my $buf    = '';
    my $stddir = File::Spec->rel2abs( File::Spec->curdir() );
    my $tmpdir = File::Temp->newdir();

    # Copy the ROOT/ref/$inpath directory into $tmpdir
    my @files;
    my $want = sub {
        make_path( File::Spec->catdir( $tmpdir, $File::Find::dir ) );
        copy( $_, File::Spec->catfile( $tmpdir, $File::Find::name ) );
    };
    find( $want, $inpath );

    die "Not implemented";

    # Detect build system
    chdir $tmpdir;

    # Compile code

    # Execute post-compilation commands
    for my $c (@cmds) {
        push @output, "42sh$ $c->[0]" unless $c->[1];
        run( command => $c->[0], buffer => \$buf, timeout => $FACT_TIMEOUT );
        push @output, map { ( ' ' x 4 ) . $_ } split /\n/ => $buf
          unless $c->[1];
    }
    chdir $stddir;
    return join "\n" => @output;
}

sub list_includes {
    my ( $in, $filename ) = @_;

    if ( defined $filename ) {
        $INCLUDES->{$filename} = 1;
        my @tmp = File::Spec->splitpath($filename);
        if ( File::Spec->file_name_is_absolute($filename) ) {
            $CURRENT_DIR = $tmp[1];
        }
        else { $CURRENT_DIR = $tmp[1] if @tmp > 2 }
    }

    $TOKEN_STREAM          = [];
    $ESCAPED               = 0;

    return unless defined $in;

    config_from_input( \$in );
    1 while $in =~ s/ \A\s*\n | (?<=\n) \s+\z //x;

    list_tokenize( $&, \$in )
      while $in =~ m/\G (?: $rESC .*? | .+? ) (?:(?=$rESC)|\z)/xgs;
    flush($ESC) if $ESCAPED;

    return [ sort keys %$INCLUDES_ALL ];
}

sub mpp {
    my ( $in, $filename ) = @_;

    unless ($REPOSITORY_ROOT = get_repository_root() or $ACCEPT_OUT_REPOSITORY) {
        error "Can't be used from the outside of a project repository"
    }

    if ( defined $filename ) {
        $INCLUDES->{$filename} = 1;
        my @tmp = File::Spec->splitpath($filename);
        if ( File::Spec->file_name_is_absolute($filename) ) {
            $CURRENT_DIR = $tmp[1];
        }
        else { $CURRENT_DIR = $tmp[1] if @tmp > 2 }
    }

    $TOKEN_STREAM          = [];
    $ESCAPED               = 0;

    return unless defined $in;

    config_from_input( \$in );
    1 while $in =~ s/ \A\s*\n | (?<=\n) \s+\z //x;

    tokenize( $&, \$in )
      while $in =~ m/\G (?: $rESC .*? | .+? ) (?:(?=$rESC)|\z)/xgs;
    flush($ESC) if $ESCAPED;

    return process_tokens();
}

sub config_from_input {
    return unless $ACCEPT_INPUT_CONFIG;
    my $stringref = shift;
    my %name2var  = (
        escape    => \&escape,
        separator => \&separator,
    );
    while ( $$stringref =~ s/\A \s* mpp_(\w+): \s* (\S+) \s* $//xim ) {
        if ( exists $name2var{$1} ) { $name2var{$1}->($2) }
        else { error qq{unknown configuration directive "mpp_$1" [removed]} }
    }
    return rebuild_regexes();
}

sub list_tokenize {
    my ( $str, $ref_in ) = @_;

    # Escaping the escape sequence
    if ($ESCAPED) {
        $str = substr( $str, length $ESC );
        flush($ESC);
    }
    elsif ( $str eq $ESC ) {
        $ESCAPED = 1;
        return;
    }

    # Macro definition?
    if ( $ACCEPT_DEFINE && $str =~ s/ ^ $rESC define \s+ //ix ) {
        $str =~ s/ \A .*? $rCLOSE_MACRO //xs;
    }

    # Macro call (with argument list)?
    elsif ( $str =~ / ^ $rcKEYWORD \( /x ) {

        my $name = $1;
        $str =~ s/ ^ $rKEYWORD //x;
        my $args = eval { extract_args( \$str ) };
        error qq{unable to parse argument list when calling "$name": $@} if $@;

        my $lname = lc $name;
        $lname = $USER_ALIASES->{$lname}->target
          if exists $USER_ALIASES->{$lname};
        if ( $lname eq 'cpp-include' || $lname eq 'scoped-include' ) {

            # This code comes from &include and, if changed, should continue
            # to follow the same behavior than &include.
            my $filename = File::Spec->canonpath( $args->[0] );
            die qq{"$filename" is an absolute path and thus rejected\n}
              if !$ACCEPT_ABSOLUTE_PATHS
              && File::Spec->file_name_is_absolute($filename);
            $INCLUDES_ALL->{$filename} += 1;
            $filename = File::Spec->catpath( $CURRENT_DIR, $filename );

            if ( exists $INCLUDES->{$filename} ) {
                die qq{file "$filename" already included; to avoid }
                  . qq{infinite recursion, such further includes are removed\n};
            }
            else { $INCLUDES->{$filename} = 1 }
            my $text = do {
                open my $fh, '<', $filename
                  or die qq{unable to read "$filename": $!\n};
                local $/;
                <$fh>;
            };

            list_includes($text, $filename);

        }
        elsif ( $lname eq 'alias' ) {
            my $correct = $lname eq 'alias' ? $str =~ /^\n/ : 0;
            pos($$ref_in) = pos($$ref_in) - length($str) + $correct;
            return;
        }
    }

    # Macro call (without argument list)?
    elsif ( $str =~ / ^ $rcKEYWORD \b /x ) {
        $str =~ s/ ^ $rKEYWORD //x;
    }

    # Tokenize the remaining text as standard (i.e. non-escaped) text
  REMAINING: { flush($str) }
    return;
}

sub tokenize {
    my ( $str, $ref_in ) = @_;

    # Escaping the escape sequence
    if ($ESCAPED) {
        $str = substr( $str, length $ESC );
        flush($ESC);
    }
    elsif ( $str eq $ESC ) {
        $ESCAPED = 1;
        return;
    }

    # Macro definition?
    if ( $ACCEPT_DEFINE && $str =~ s/ ^ $rESC define \s+ //ix ) {
        my ( $name, $argslist ) = $str =~ m/
            ^ ($rWORD) \( \s*
                ( $rcPARAM (?: \s* $rSEP \s* $rcPARAM )* )?
            \s* \)
        /mxsi;

        my @arglist = ($argslist || '') =~ / $rcPARAM /gx;

        if ( @$TOKEN_STREAM && $TOKEN_STREAM->[0]{type} eq 'std' ) {
            $TOKEN_STREAM->[0]{value} =~ s/ \h* $ //x;
        }
        @arglist = grep { defined } @arglist;
        unless ( defined $name ) {
            ( my $hint = $str ) =~ s/\s+/ /g;
            $hint = substr( $hint, 0, length $str < 15 ? length $str : 15 );
            error "unrecognized macro prototype (starting with "
              . qq{"$hint ...") removed until "$CLOSE_MACRO" (if any) or }
              . q{next escape};
            $str =~ s/ \A .*? $rCLOSE_MACRO //xsm;
            goto REMAINING;
        }
        my ( $body, $endmacro ) = $str =~ / \) \s* (.+?) ($rCLOSE_MACRO) /xs;
        unless ( defined $body ) {
            my $hint;
            if ( $str =~ / \) .+? $rCLOSE_MACRO_FAIL /xs ) {
                $hint = q{did you failed to close it with }
                  . qq{single-line "$CLOSE_MACRO"?};
                $str =~ s/ \A .*? $rCLOSE_MACRO_FAIL //xsm;
            }
            else {
                $hint = qq{no end-of-body token "$CLOSE_MACRO" found};
                $str =~ s/ \A .*? $rCLOSE_MACRO //xsm;
            }
            error qq{unrecognized body for macro "$name" ($hint) [removed]};
            goto REMAINING;
        }
        error qq{body of macro "$name" spans until next escape }
          . q{or EOF [not removed]}
          if $endmacro eq '';
        $str =~ s/ \A .*? $rCLOSE_MACRO //xs;
        push @$TOKEN_STREAM,
          {
            type => 'macro',
            name => 'define',
            args => [ $name, \@arglist, $body ],
          };
    }

    # Macro call (with argument list)?
    elsif ( $str =~ / ^ $rcKEYWORD \( /x ) {
        my $name = $1;
        $str =~ s/ ^ $rKEYWORD //x;
        my $args = eval { extract_args( \$str ) };
        error qq{unable to parse argument list when calling "$name": $@} if $@;

        push @$TOKEN_STREAM, { type => 'macro', name => $name, args => $args };

        my $lname = lc $name;
        $lname = $USER_ALIASES->{$lname}->target
          if exists $USER_ALIASES->{$lname};
        if ( $lname eq 'cpp-include' || $lname eq 'alias' ) {
            my $intermediary = process_tokens();
            chomp $intermediary;
            $TOKEN_STREAM = [ { type => 'std', value => $intermediary } ];

            # Need to step back a little so that the regex that manages
            # tokenize() calls would recognize possibly new escapes.
            my $correct = $lname eq 'alias' ? $str =~ /^\n/ : 0;
            pos($$ref_in) = pos($$ref_in) - length($str) + $correct;
            return;
        }
    }

    # Macro call (without argument list)?
    elsif ( $str =~ / ^ $rcKEYWORD \b /x ) {
        push @$TOKEN_STREAM, { type => 'macro', name => $1, args => [] };
        $str =~ s/ ^ $rKEYWORD //x;
    }

    # Tokenize the remaining text as standard (i.e. non-escaped) text
  REMAINING: { flush($str) }
    return;
}

sub extract_args {
    my $str_ref = shift;

    my ($open_using) = $$str_ref =~ m/ ^ (.) /xgs;
    die qq{can't use "$open_using" as opening character for argument list\n}
      unless exists $GROUPING{$open_using};

    my ( $group_level, $need_eval, $quoted_using, $grouped_using ) = ( 0, 0 );
    my ( $s, $buffer, $c, @args ) = ( 'std', '' );

    pos($$str_ref) = 1;
    while ( $$str_ref =~ m/ \G (?: $rESC | $rSEP | . ) /xgs ) {
        $c = $&;

        if ( $s eq 'std' ) {
            if ( $c eq $ESC ) { $s = 'std_esc' }
            elsif ( exists $QUOTING{$c} ) {    # opening quote
                $s            = 'quoted';
                $quoted_using = $c;
                $buffer .= $c;
                $need_eval = 1;
            }
            elsif ( exists $GROUPING{$c} ) {    # opening group
                if ( $group_level == 0 ) {      # first-level group
                    $group_level   = 1;
                    $grouped_using = $c;
                    $need_eval     = 1;
                }
                elsif ( $c eq $grouped_using ) {    # subgroup
                    $group_level += 1;
                }
                $buffer .= $c;
            }

            # closing group
            elsif ( $group_level > 0 && $c eq $GROUPING{$grouped_using} ) {
                $group_level -= 1;
                $buffer .= $c;
            }

            # closing arglist
            elsif ( $group_level == 0 && $c eq $GROUPING{$open_using} ) {
                push @args, [ $buffer, $need_eval ];
                last;
            }
            elsif ( $group_level == 0 && $c eq $SEP ) {
                push @args, [ $buffer, $need_eval ];
                $buffer    = '';
                $need_eval = 0;
            }
            else { $buffer .= $c unless $c =~ /^\s$/ }
        }
        elsif ( $s eq 'std_esc' ) {
            $buffer .= $c;
            $s = 'std';
        }
        elsif ( $s eq 'quoted' ) {
            if ( $c eq $ESC ) { $s = 'quoted_esc'; $buffer .= '\\' }
            elsif ( $c eq $QUOTING{$quoted_using} ) {
                $s = 'std';
                $buffer .= $c;
            }
            else { $buffer .= $c }
        }
        elsif ( $s eq 'quoted_esc' ) {
            $buffer .= $c;
            $s = 'quoted';
        }
        else { die "shouldn't be here!" }
    }

    die "argument list parsing failed\n" unless defined pos($$str_ref);
    if ( pos($$str_ref) <= length($$str_ref) ) {
        $$str_ref = substr $$str_ref, pos($$str_ref);
    }
    else { $$str_ref = '' }

    @args = map { $_->[1] ? $SAFE->reval( $_->[0] ) : $_->[0] } @args;
    return \@args;
}

sub process_tokens {
    my $output = '';
    while ( my $t = shift @$TOKEN_STREAM ) {
        if ( $t->{type} eq 'macro' ) {
            my ( $name, $lname ) = ( $t->{name}, lc $t->{name} );
            my @args = @{ $t->{args} };

            if ( exists $USER_ALIASES->{$lname} ) {
                $lname = $USER_ALIASES->{$lname}->target;
            }

            if ( exists $USER_MACROS->{$lname} ) {
                my $result =
                  eval { run_user_macro( $lname, $TOKEN_STREAM, @args ) } // '';
                if ($@) {
                    $@ =~ s/\s*$//;
                    error qq{when executing "$name" ($@) [removed]};
                }
                else { $output .= $result // '' }
            }
            elsif ( exists $DEFAULT_MACROS->{$lname} ) {
                my $result = eval { $DEFAULT_MACROS->{$lname}->( \@args ) };
                if ($@) {
                    chomp $@;
                    error qq{when executing "$name" ($@) [removed]};
                }
                else { $output .= $result // '' }
            }
            else {
                error qq{call to unknown macro "$name" [not removed]};
                $output .= $ESC . $name;
                $output .= '(' . join( $SEP, @args ) . ')' if @args;
            }
        }
        else { $output .= $t->{value} }
    }
    return $output;
}

sub flush {
    my $str = shift;
    return if $str eq '';
    if ( defined $TOKEN_STREAM->[-1] and $TOKEN_STREAM->[-1]{type} eq 'std' ) {
        $TOKEN_STREAM->[-1]{value} .= $str;
    }
    else { push @$TOKEN_STREAM, { type => 'std', value => $str } }
    $ESCAPED = 0;
    return 1;
}

sub run_user_macro {
    my ( $name, $STACK, @args ) = @_;
    my $macro = $USER_MACROS->{$name};
    my $safe  = new Safe;

    for my $param ( @{ $macro->arglist } ) {
        my $arg = shift @args;
        no strict qw( refs vars );    ## no critic
        ${"$param"} = $arg;
        $safe->share("\$$param");
    }

    $safe->share('$STACK');
    my $result = $safe->reval( $macro->body, 'use strict' );
    if ($@) { $@ =~ s/ at .+? line \d+\.\s*$//; die "Safe: $@\n" }
    else    { return $result }
    return;
}

1;
__END__

=head1 NAME

MPP - A Markdown preprocessor

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 SUBROUTINES

=head2 C<mpp>

=head2 C<accept_define>

By default, C<mpp> allows its input to define new macros (using the I<define>
builtin). If you call C<accept_define> with a false value, subsequent calls to
C<mpp> will reject any use of the I<define> builtin until the next call to
C<accept_define> or the end of the program.

=head1 DIAGNOSTICS

=head1 DEPENDENCIES

=head1 AUTHOR

Thibaut Le Page <lepage_a@acu.epita.fr>, ACU qualité 2014

=head1 LICENSE AND COPYRIGHT

The MIT License (MIT)

Copyright (c) 2013 Thibaut Le Page

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
