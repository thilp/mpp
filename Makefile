TESTDIR=t
PROVER=prove -f
INSTALLDIR=/sgoinfre/butler/mpp

test:
	$(PROVER) $(TESTDIR)

testv:
	$(PROVER) -v $(TESTDIR)

install: test
	cp --backup=numbered   MPP.pm             $(INSTALLDIR)
	-cp --backup=numbered  $(INSTALLDIR)/mpp  $(INSTALLDIR)
	perl -pe "s|^use MPP.*|use lib '$(INSTALLDIR)';\n$$&|" mpp > $(INSTALLDIR)/mpp
	chmod +x $(INSTALLDIR)/mpp
