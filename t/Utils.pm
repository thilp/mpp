package Utils;

use 5.010_001;
use strict;
use warnings;

use Class::Struct FileTree => { name => '$', includes => '@' };
use File::Temp qw(tempfile);
use Digest::MD5 qw(md5);
use Storable qw(dclone);

my $INDEX;

BEGIN {
    use parent 'Exporter';
    our @EXPORT;
    our %EXPORT_TAGS = (
        filetree => [qw( test_on_filetree )],
        warnings => [qw( warnings_like )],
    );

    $EXPORT_TAGS{all} = do {
        my %seen;
        for my $tag (keys %EXPORT_TAGS) {
            $seen{$_} = 1 for split ' ' => $EXPORT_TAGS{$tag};
        }
        [ keys %seen ]
    };
    Exporter::export_ok_tags($_) for keys %EXPORT_TAGS;
}

BEGIN { require Test::More }
BEGIN {

    # Makes Test::More report errors in files that use Utils instead of here
    # (Test::More uses caller(1) instead of caller(2))
    sub _carp {
        my ( $file, $line ) = ( caller(2) )[ 1, 2 ];
        return warn @_, " at $file line $line\n";
    }
    *Test::More::_carp = *_carp;

    sub skip_wrapper (&$;$) {
        Test::More::skip "because Test::Warn is unavailable", 1;
    }
    eval { require Test::Warn; import Test::Warn };
    *warning_like = *skip_wrapper if $@;
}

BEGIN {
    eval { require Test::Differences };
    if (!$@) {
        import Test::Differences qw(eq_or_diff);
        *Test::More::is = *Test::Differences::eq_or_diff;
    }
}

sub build_filetree {
    my $node = shift;
    my $fp = md5( $node->{text} );

    return FileTree->new( name => $INDEX->{$fp} )
      if exists $node->{inc} && $INDEX->{$fp};

    my $index = dclone($INDEX);

    my $filetree = FileTree->new();
    my $text = $node->{text} or die '$node->{text} = undef';
    my ($fh, $fn) = tempfile();
    $filetree->name($fn);
    $INDEX->{$fp} = $fn;

    for my $inc (@{ $node->{inc} }) {
        my $subtree = build_filetree($inc);
        $INDEX = $index;
        push @{ $filetree->includes() }, $subtree;
        $text =~ s/__FILE__/$subtree->name/e;
    }

    print {$fh} $text;
    close $fh;
    return $filetree;
}

sub destroy_filetree {
    my $ft = shift or return;
    destroy_filetree($_) for @{ $ft->includes };
    unlink $ft->name if -w $ft->name;
}

sub test_on_filetree {
    use MPP qw(mpp reset_mpp reset_mpp_hard accept_absolute_paths);
    reset_mpp_hard();
    accept_absolute_paths(1);

    Test::More::note('these tests will try to create some temporary files');
    my $T = shift();
    foreach my $test ( keys %$T ) {
        $INDEX = {};
        my $ft   = build_filetree( $T->{$test}{'root'} );
        my $text = do {
            die $! unless open my $fh, '<', $ft->name;
            local $/;
            <$fh>;
        };
      SKIP: {
          no warnings 'MPP';
            reset_mpp();
            Test::More::is(
                mpp($text, $ft->name),
                $T->{$test}{expect},
                $test . (
                    exists $T->{$test}{descr}
                    ? ': ' . $T->{$test}{descr}
                    : ''
                )
            );
          use warnings 'MPP';
            reset_mpp();
            warning_like { mpp($text) } $T->{$test}{warn},
              $test . ' '
              . ( $T->{$test}{warn} ? "warns as expected" : "doesn't warn" );
        }
        destroy_filetree($ft);
    }

    reset_mpp_hard();
    $INDEX = {};
    return;
}

1;
