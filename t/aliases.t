use 5.010_001;
use strict;
use warnings;

use lib 't';
use Utils qw(:filetree :warnings);

my $T;

BEGIN {
    $T = {
        'simple' => {
            root   => { text => "%%define A() 2\n=end\n%%alias(B, A)%%A%%B" },
            expect => "22"
        },
        alias_unknown_macro => {
            root   => { text => "%%alias(A, B)%%A\n" },
            expect => "%%A\n",
            warn   => [
                qr/\b[Uu]nknown macro "B"/, qr/\b[Cc]all to unknown macro "A"/
            ],
        },
        'alias_cpp-include' => {
            root => {
                text => "%%alias( totoro, cpp-include )\n"
                  . "%%totoro(__FILE__)",
                inc => [ { text => "%SAFE%" } ],
            },
            expect => "%SAFE%"
        },
        same_name => {
            root => {
                text => "%%define A()7\n=end\n".
                "%%alias(a,A)%%a"
            },
            expect => "7",
            warn => qr/\balias and target name must be different\b/,
        },
        change_alias => {
            root => {
                text => "%%define A()7\n=end\n%%define B()9\n=end\n".
                "%%alias(c,A)%%alias(c,B)%%c"
            },
            expect => "9",
            warn => qr/\balias "c" retargeted from "A" to "B"/,
        },
        multi_aliases => {
            root => {
                text => "%%define A()7\n=end\n".
                "%%alias(b,A)\n%%alias(c,A)\n%%c%%b"
            },
            expect => "77"
        },
        scoped_alias => {
            root => {
                text => "%%define A() 2\n=end\n"
                  . "%%scoped-include(__FILE__)%%b",
                inc => [ { text => "%%alias( b, A )\n" } ],
              },
            expect => "%%b",
            warn => qr/\b[Uu]nknown macro "b"/,
            descr => "aliases are restored when leaving a scoped-include",
        },
        unscoped_alias => {
            root => {
                text => "%%define A() 2\n=end\n"
                  . "%%cpp-include(__FILE__)%%b",
                inc => [ { text => "%%alias( b, A )\n" } ],
              },
            expect => "2",
            descr => "aliases persists when leaving a cpp-include",
        },
    };
}

use Test::More tests => 2 * keys(%$T);
use MPP qw(:all);
escape('%%');

test_on_filetree($T);
