use 5.010_001;
use strict;
use warnings;

BEGIN { our @escapes = qw(~~ %% % --- %@ 3 33 333 3333) }

use Test::More tests => 9 * our @escapes + 7;

BEGIN {
    diag("Testing MPP on perl $]");
    use_ok('MPP', ':all');
}

can_ok( 'MPP', qw( mpp escape ) );

escape('~~');

cmp_ok(escape(), 'eq', '~~', 'escape() works');

is( mpp(''), '', 'leave empty string unchanged' );

is( mpp( " abc** **abc" x 5 ), " abc** **abc" x 5,
    'leave standard text unchanged' );

is(
    mpp("mpp_escape: **\n\n******\n"),
    "****\n",
    'use a custom escape sequence'
);

is(escape(), '**', 'escape is not reinitialized after mpp()');

for my $esc ( our @escapes ) {
    escape($esc);
    my $msg = " (ESC = '$esc')";

    is( mpp($esc), $esc,
        'leave $MPP::ESC unchanged when alone' . $msg );

    is( mpp("$esc "), "$esc ",
        'leave $MPP::ESC unchanged when followed by blank' . $msg );

    is(
        mpp(" $esc "),
        " $esc ",
        'leave $MPP::ESC unchanged when preceded and followed by blank' . $msg
    );

    is( mpp( $esc x 2 ),
        $esc, 'translate $MPP::ESC x 2 into $MPP::ESC' . $msg );

    {
        my $word = $esc;
        my $wo = substr( $esc, 0, length($esc) / 2 );

        is(
            mpp( $word . $wo ),
            $word . $wo,
            'word.wo unchanged when word is escape' . $msg
        );

        is(
            mpp( ( $word x 2 ) . $wo ),
            $word . $wo,
            'word.word.wo -> word.wo when word is escape' . $msg
        );

        is(
            mpp( ( $word x 3 ) . $wo ),
            ( $word x 2 ) . $wo,
            'word.word.word.wo -> word.word.wo when word is escape' . $msg
        );
    }

    is(
        mpp( $esc x 2000 ),
        $esc x 1000,
        'translate $MPP::ESC x 2000 into $MPP::ESC x 1000' . $msg
    );
    is(
        mpp( $esc x 2001 ),
        $esc x 1001,
        'translate $MPP::ESC x 2001 into $MPP::ESC x 1001' . $msg
    );
}
