use 5.010_001;
use strict;
use warnings;

use lib 't';
use Utils qw(:filetree :warnings);

my $T;
BEGIN {
    $T = {
        simple => {
            root => {
                text => "%%cpp-include(__FILE__)\n",
                inc =>
                  [ { text => "aaaaa %%%\n%%DEFINE a() 4\n=end\no%%a\n" } ],
            },
            expect => "aaaaa %%%\no4\n"
        },
        two_levels => {
            root => {
                text => "auie%%cpp-include(__FILE__)nrst",
                inc  => [
                    {
                        text => "kpa--#?p%%cpp-include(__FILE__)\n",
                        inc  => [ { text => "\n\nDTD\n\n\n~" } ],
                    },
                ],
            },
            expect => "auiekpa--#?pDTD\n\n\n~nrst"
        },
        multiple_includes => {
            root => {
                text => "%%cpp-include(__FILE__)%%cpp-include(__FILE__)33",
                inc  => [ { text => "333" }, { text => "333" } ],
            },
            expect => '33333333'
        },
        simple_after => {
            root => {
                text => "%%cpp-include(__FILE__)%%A",
                inc  => [ { text => "%%define A() 4\n=end" } ],
            },
            expect => "4",
            warn   => undef,
            descr  => 'macro call after included macro definition works'
        },
        simple_sides => {
            root => {
                text => "%%A%%cpp-include(__FILE__)%%A",
                inc  => [ { text => "%%define A() 4\n=end" } ],
            },
            expect => "%%A4",
            warn   => qr/[uU]nknown macro "A"/,
            descr  => 'macro call before included macro definition is verbatim'
        },
        escape_1 => {
            root => {
                text => "%%cpp-include(__FILE__)~~A",
                inc =>
                  [ { text => "mpp_escape: ~~\n~~define A() 4\n=end\n~~A" } ],
            },
            expect => "44",
            warn   => undef,
            descr  => 'ESCAPE is changed in includer file'
        },
        escape_2 => {
            root => {
                text => "%%cpp-include(__FILE__)%%A",
                inc =>
                  [ { text => "mpp_escape: ~~\n~~define A() 4\n=end\n~~A" } ],
            },
            expect => "4%%A",
            warn   => undef,
            descr  => 'old ESCAPE is not recognized anymore in includer file'
        },
        cyclic_3 => {
            root => {
                text => "mpp_escape: +++\na~~+++cpp-include(__FILE__)\n",
                inc  => [
                    {
                        text => "mpp_escape: %\n%%%cpp-include(__FILE__)\$\n",
                        inc  => [
                            {
                                text => "mpp_escape: _|\nc_|cpp-include"
                                  . "(__FILE__)~~%cpp-include\n",
                                inc => []
                            }
                        ]
                    }
                ],
            },
            expect => "a~~%c~~%cpp-include\$\n",
            descr  => 'triangular dependency handling',
            warn   => qr/infinite recursion/i
        },
        cruel => {
            root => {
                text => <<'EOF',
mpp_escape: ~~
~~~~~%%
    ~~define K($a, $b) $a.$b
    =end
~~K('(cou', 'cou)')
~~cpp-include(__FILE__)
~~K('ça ','va ?')
*K("ta","ti")
EOF
                inc => [
                    {
                        text => <<'EOF',
~~K("to", "toro")
~~define K($a, $b)
    $b.$a
=end
~~K("to", "toro")
%%cpp-include(/tmp/test)
~~cpp-include(__FILE__)
~~K("to", "toro")
*cpp-include(__FILE__)
~~K("to", "toro")
*K("ta", "ti")
~~swift
EOF
                        inc => [
                            { text => <<'EOF' },
mpp_escape: *
~~K("bla", "bla.")
*K("bla", "bla.")
*define K() 42
=end
*K("bla", "bla")
EOF
                            { text => <<'EOF' } ],
~~K("U", "AC")
*K("KA", "YA")
~~define swift() 2
=end
EOF
                    },
                ],
            },
            expect => <<'EOF',
~~~%%
(coucou)
totoro
toroto
%%cpp-include(/tmp/test)
~~K("bla", "bla.")
bla.bla
42
~~K("to", "toro")
~~K("U", "AC")
42
~~define swift() 2
=end
~~K("to", "toro")
42
~~swift
~~K('ça ','va ?')
42
EOF
            warn => [
                qr/\bonly the last definition of "K"/i,
                qr/\bonly the last definition of "K"/i,
            ]
        },
    };

    push @{$T->{cyclic_3}{root}{inc}[0]{inc}[0]{inc}}, $T->{cyclic_3}{root};
}

use Test::More tests => 2 * keys(%$T);

use MPP qw(:all);
escape('%%');

test_on_filetree($T);
