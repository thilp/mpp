use 5.010_001;
use strict;
use warnings;
use Test::More tests => 5;

use MPP ':all';

escape('~~');

is( mpp('~~ date'), '~~ date', 'invalid date call included verbatim' );

is(
    mpp('~~date'),
    scalar localtime(),
    '"~~date" is the same as "scalar localtime()"'
);

is( mpp( '~~date' x 3 ), localtime() x 3, 'multiple calls to "~~date"' );

is(
    mpp( '~~date ' x 3 ),
    ( localtime() . ' ' ) x 3,
    'multiple calls to "~~date", whitespace separated'
);

is(
    mpp('~~date()'),
    scalar localtime(),
    '"~~date()" is the same as "scalar localtime()"'
);
