use 5.010_001;
use strict;
use warnings;
use Test::More tests => 7;

use MPP ':all';

no warnings 'MPP';

can_ok('MPP', 'reset_mpp', 'escape');

escape('~~');

is(
    mpp(<<'EOF'),
~~define PLUS($a, $b)
    $a + $b
=end
~~PLUS(21, 21)
EOF
    "42\n",
    'can define an addition'
);

reset_mpp();
escape('~~');
is(
    mpp(<<'EOF'),
~~define PLUS($a, $b)
    $a + $b
=end
PLUS(21, 21)
EOF
    "PLUS(21, 21)\n",
    'macro call without escape is ignored'
);

reset_mpp();
escape('~~');
is(
    mpp(<<'EOF'),
~~define PLUS($a, $b)
    $a + $b
=end
EOF
    '',
    'macro definition (only) produces nothing'
);

reset_mpp();
escape('~~');
is(
    mpp(<<'EOF'),
~~define PLUS(a, b)
    $a + $b
=end
EOF
    '',
    'invalid macro prototype is ignored and produces nothing'
);

reset_mpp();
escape('~~');
is(
    mpp(<<'EOF'),
~~define PLUS(a, b)
    $a + $b
=end
~~PLUS(21, 21)
EOF
    "~~PLUS(21,21)\n",
    'bad macro definition followed by macro call results in verbatim macro call'
);

reset_mpp();
escape('~~');
is(
    mpp(<<'EOF'),
~~define HAXXOR()
    print system('date')
=end
~~HAXXOR
EOF
    "\n",
    'call to macro which tries to break Safe produces nothing'
);
