use 5.010_001;
use strict;
use warnings;

use lib 't';
use Utils qw(:filetree :warnings);

my $T;

BEGIN {
    $T = {
        simple => {
            root => {
                text => "%%display-code(__FILE__, perl)\n",
                inc => [{ text => "sub myfunc {\n3 + shift\n}\n" }],
            },
            expect => "\n<!---->\n\n".('~' x 10) . " { .perl .numberLines startFrom=\"1\" }\n"
            . "sub myfunc {\n3 + shift\n}\n"
            . ('~' x 30) . "\n\n<!---->\n"
        },
        simple_lines => {
            root => {
                text => qq{%%display-code(__FILE__, perl, [ "1-2" ])\n},
                inc => [{ text => "sub myfunc {\n3 + shift\n}\n" }],
            },
            expect => "\n<!---->\n\n".('~' x 10) . " { .perl .numberLines startFrom=\"1\" }\n"
            . "sub myfunc {\n3 + shift\n"
            . ('~' x 30) . "\n\n<!---->\n"
        },
        double_lines => {
            root => {
                text => qq{%%display-code(__FILE__, perl, [ "1-2", "2-3" ])\n},
                inc => [{ text => "sub myfunc {\n3 + shift\n}\n" }],
            },
            expect =>
            "\n<!---->\n\n".('~' x 10) . " { .perl .numberLines startFrom=\"1\" }\n"
            . "sub myfunc {\n3 + shift\n"
            . ('~' x 30) ."\n"
            . "\n"
            . ('~' x 10) . " { .perl .numberLines startFrom=\"2\" }\n"
            . "3 + shift\n}\n"
            . ('~' x 30) . "\n\n<!---->\n"
        },
        overflow_lines => {
            root => {
                text => qq{%%display-code(__FILE__, perl, [ "0-27" ])\n},
                inc => [{ text => "sub myfunc {\n3 + shift\n}\n" }],
            },
            expect => "\n<!---->\n\n".('~' x 10) . " { .perl .numberLines startFrom=\"1\" }\n"
            . "sub myfunc {\n3 + shift\n}\n"
            . ('~' x 30) . "\n\n<!---->\n"
        },
        error_lines => {
            root => {
                text => qq{%%display-code(__FILE__, perl, [ "3-1" ])\n},
                inc => [{ text => "sub myfunc {\n3 + shift\n}\n" }],
            },
            expect => "\n",
            warn => qr/\binvalid linespec "3-1": 3 > 1/,
        },
    };
}

use Test::More tests => 2 * keys(%$T);
use MPP qw(:all);
escape('%%');

test_on_filetree($T);
