use 5.010_001;
use strict;
use warnings;

my $T;
BEGIN {
    $T = {
        simple => {
            text => "(abc,def)",
            ref => [ "abc", "def" ],
        },
        simple_ws => {
            text => "(abc, def)",
            ref => [ "abc", "def" ],
        },
        simple_after_ws => {
            text => "(abc,def) ",
            ref => [ "abc", "def" ],
            after => ' ',
        },
        simple_ws_left_1 => {
            text => "( abc,def) ",
            ref => [ "abc", "def" ],
            after => ' ',
        },
        simple_ws_left_2 => {
            text => "(abc ,def) ",
            ref => [ "abc", "def" ],
            after => ' ',
        },
        simple_ws_left_3 => {
            text => "( abc ,def) ",
            ref => [ "abc", "def" ],
            after => ' ',
        },
        simple_ws_right => {
            text => "(abc,def ) ",
            ref => [ "abc", "def" ],
            after => ' ',
        },
        simple_after_lf => {
            text => "(abc,def)\nlol",
            ref => [ "abc", "def" ],
            after => "\nlol",
        },
        simple_dontcareafter => {
            text => '(abc, def))(',
            ref => [ 'abc', 'def' ],
            after => ')(',
        },
        quoted_simple_dquotes => {
            text => '(abc, "de,f)")',
            ref => [ 'abc', 'de,f)' ],
        },
        quoted_simple_parens => {
            text => '("(abc", "def)")',
            ref => [ '(abc', 'def)' ],
        },
        quoted_simple_squotes => {
            text => '(\'abc\', def)',
            ref => [ 'abc', 'def' ],
        },
        quoted_simple_arrayref => {
            text => '([ "ab", "c" ], def)',
            ref => [ [ 'ab', 'c' ], 'def' ],
        },
        quoted_simple_hashref => {
            text => '(abc, { "d", "ef" })',
            ref => [ 'abc', { d => 'ef' } ],
        },
        quoted_nested => {
            text => '([ "[ab]", "]c" ], "d(ef")',
            ref => [ [ '[ab]', ']c' ], 'd(ef' ],
        },
        quoted_escaped_1 => {
            text => '(abc, "%%"def")',
            ref => [ 'abc', '"def' ],
        },
        quoted_escaped_2 => {
            text => '(abc, de%%)f)',
            ref => [ 'abc', 'de)f' ],
        },
        display_code => {
            text => qq{(/tmp/auiea7, perl, [ "3-1" ])\n},
            ref => [ '/tmp/auiea7', 'perl', [ '3-1' ] ],
            after => "\n",
        },
        display_code_after => {
            text => qq{(/tmp/auiea7, perl, [ "3-1" ])~~lol},
            ref => [ '/tmp/auiea7', 'perl', [ '3-1' ] ],
            after => "~~lol",
        },
    };
}

use Test::More tests => 2 * keys %$T;
BEGIN {
    if ( !eval q{ use Test::Differences; 1 } ) {
        *eq_or_diff = \&is_deeply;
    }
}
use MPP qw(:_dev);

for my $t ( keys %$T ) {
    my $str = $T->{$t}{text};
    eq_or_diff( extract_args(\$str), $T->{$t}{ref}, "$t: arglist" );
    $T->{$t}{after} = '' unless exists $T->{$t}{after};
    is($str, $T->{$t}{after}, "$t: remaining string");
}

done_testing();
