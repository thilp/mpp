use 5.010_001;
use strict;
use warnings;

use Test::More;

use MPP ':_dev';

{
    my %cfi = (
        escape    => 'ESC',
        separator => 'SEP',
    );
    no warnings 'MPP';
    while ( my ( $name, $var ) = each %cfi ) {
        no strict 'refs';
        "$name"->('kkkkkkkkk');
        my $str = "mpp_$name: 124\nmpp_$name: 123";
        config_from_input( \$str );
        cmp_ok("$name"->(), 'eq', '123',
            "config_from_input works for $var" );
    }
}

done_testing();
