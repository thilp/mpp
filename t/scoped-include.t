use 5.010_001;
use strict;
use warnings;

use lib 't';
use Utils qw(:filetree :warnings);

my $T;

BEGIN {
    $T = {
        simple => {
            root => {
                text => "%%scoped-include(__FILE__)\n",
                inc =>
                  [ { text => "aaaaa %%%\n%%DEFINE a() 4\n=end\no%%a\n" } ],
            },
            expect => "aaaaa %%%\no4\n"
        },
        two_levels => {
            root => {
                text => "auie%%scoped-include(__FILE__)nrst",
                inc  => [
                    {
                        text => "kpa--#?p%%scoped-include(__FILE__)\n",
                        inc  => [ { text => "\n\nDTD\n\n\n~" } ],
                    },
                ],
            },
            expect => "auiekpa--#?pDTD\n\n\n~nrst"
        },
        multiple_includes => {
            root => {
                text =>
                  "%%scoped-include(__FILE__)%%scoped-include(__FILE__)33",
                inc => [ { text => "333" }, { text => "333" } ],
            },
            expect => '33333333'
        },
        simple_after => {
            root => {
                text => "%%scoped-include(__FILE__)%%A",
                inc  => [ { text => "%%define A() 4\n=end" } ],
            },
            expect => "%%A",
            warn   => qr/[uU]nknown macro "A"/,
            descr  => 'macro call after included macro definition works'
        },
        simple_sides => {
            root => {
                text => "%%A%%scoped-include(__FILE__)%%A",
                inc  => [ { text => "%%define A() 4\n=end" } ],
            },
            expect => "%%A%%A",
            warn   => [ qr/[uU]nknown macro "A"/, qr/[uU]nknown macro "A"/ ],
            descr  => 'macro call before included macro definition is verbatim'
        },
        escape_1 => {
            root => {
                text => "%%scoped-include(__FILE__)%%A",
                inc =>
                  [ { text => "mpp_escape: ~~\n~~define A() 4\n=end\n~~A" } ],
            },
            expect => "4%%A",
            warn   => qr/[uU]nknown macro "A"/,
            descr  => 'macro call uses the definition of ESCAPE of the file '
              . 'it belongs',
        },
        escape_2 => {
            root => {
                text => "%%scoped-include(__FILE__)~~A",
                inc =>
                  [ { text => "mpp_escape: ~~\n~~define A() 4\n=end\n~~A" } ],
            },
            expect => "4~~A",
            warn   => undef,
            descr => 'redefinition of ESCAPE is not propagated in includer file'
        },
        cyclic_3 => {
            root => {
                text => "mpp_escape: +++\na~~+++scoped-include(__FILE__)\n",
                inc  => [
                    {
                        text =>
                          "mpp_escape: %\nb%%%scoped-include(__FILE__)\$\n",
                        inc => [
                            {
                                text => "mpp_escape: _|\nc_|scoped-include"
                                  . "(__FILE__)~~%scoped-include\n",
                                inc => []
                            }
                        ]
                    }
                ],
            },
            expect => "a~~b%c~~%scoped-include\$\n",
            descr  => 'triangular dependency handling',
            warn   => qr/infinite recursion/i
        },
        cruel => {
            root => {
                text => <<'EOF',
mpp_escape: ~~
~~~~~%%
    ~~define K($a, $b) $a.$b
    =end
~~K('(cou', 'cou)')
~~scoped-include(__FILE__)
~~K('ça ','va ?')
*K("ta","ti")
EOF
                inc => [
                    {
                        text => <<'EOF',
~~K("to", "toro")
~~define K($a, $b)
    $b.$a
=end
~~K("to", "toro")
%%scoped-include(/tmp/test)
~~scoped-include(__FILE__)
~~K("to", "toro")
~~scoped-include(__FILE__)
~~K("to", "toro")
*K("ta", "ti")
~~swift
EOF
                        inc => [
                            { text => <<'EOF' },
mpp_escape: *
~~K("bla", "bla.")
*K("bla", "bla.")
*define K() 42
=end
*K("bla", "bla")
EOF
                            { text => <<'EOF' } ],
~~K("U", "AC")
*K("KA", "YA")
~~define swift() 2
=end
EOF
                    },
                ],
            },
            expect => <<'EOF',
~~~%%
(coucou)
totoro
toroto
%%scoped-include(/tmp/test)
~~K("bla", "bla.")
bla.bla
42
toroto
ACU
*K("KA", "YA")
toroto
*K("ta", "ti")
~~swift
ça va ?
*K("ta","ti")
EOF
            warn => [
                qr/\bonly the last definition of "K"/i,
                qr/\bonly the last definition of "K"/i,
                qr/\bunknown macro "swift"/i
              ]
        },
    };

    push @{ $T->{cyclic_3}{root}{inc}[0]{inc}[0]{inc} }, $T->{cyclic_3}{root};
}

use Test::More tests => 2 * keys(%$T);

use MPP qw(:all);
escape('%%');

test_on_filetree($T);
