use 5.010_001;
use strict;
use warnings;
use Test::More tests => 3;
use MPP ':all';

escape('~~');

my %examples = (
    dangerous_macro_called => [
        "%%define DANGEROUS()\n system('date')\n=end\n%%DANGEROUS",
        qr/when executing "DANGEROUS" \(Safe:/i
    ],
    dangerous_macro_defined =>
      [ "%%define DANGEROUS()\n system('date')\n=end", undef ],
    unknown_macro => [
        "aabba%%TOTORO(tata) hi", qr|\bunknown macro "TOTORO"|i
    ],
    builtin_overriding   => [ "%%define DATE()\n0\n=end", qr/\boverrid/i ],
    multiple_definitions => [
        "%%define A() 1\n=end\nb%%define A() 2\n=end",
        qr/only the last/i
    ],
    invalid_definition_1 =>
      [ "%%define A 1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_2 =>
      [ "%%define A\n1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_3 =>
      [ "%%define A(\n1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_4 =>
      [ "%%define A)\n1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_5 =>
      [ "%%define A)(\n1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_6 =>
      [ "%%define A()\n1\n= end", qr/\bfailed to close\b/i ],
    invalid_definition_7 =>
      [ "%%define A()\n1 =end", qr/\bfailed to close\b/i ],
    invalid_definition_8 =>
      [ "%%define A() 1 =end", qr/\bfailed to close\b/i ],
    invalid_definition_9 =>
      [ "%%define A() 1 = end", qr/\bfailed to close\b/i ],
    invalid_definition_10 =>
      [ "%%define A(a) a+1\n=end", qr/\bunrecognized macro prototype\b/i ],
    invalid_definition_11 =>
      [ "%%define A(a) 1\n=end", qr/\bunrecognized macro prototype\b/i ],
);

SKIP: {

    # Force to skip (but still compiles) for a 'Test::More' without 'subtest'
    BEGIN {
        sub do_nothing {}
        *subtest = *do_nothing unless Test::More->can('subtest');
    }
    skip "because your version of Test::More is too old for subtest()", 3
      unless Test::More->can('subtest');

    BEGIN {
        eval { require Test::Warn; import Test::Warn };
    }
    eval { require Test::Warn };
    skip 'because the Test::Warn module is not installed', 3
      if $@;

    # With warnings
    # Warnings should be activated by default, so no use warnings 'MPP' here
    subtest 'with default verbosity' => sub {
        plan tests => scalar keys %examples;
        for ( keys %examples ) {
            reset_mpp();
            warning_like { mpp( $examples{$_}[0] ) } $examples{$_}[1], $_;
        }
    };

    # Without warnings
    no warnings 'MPP';
    subtest 'when verbosity deactivated' => sub {
        plan tests => scalar keys %examples;
        for ( keys %examples ) {
            reset_mpp();
            warning_is { mpp( $examples{$_} ) } undef, $_;
        }
    };

    # Reactivate warnings
    use warnings 'MPP';
    subtest 'when verbosity reactivated' => sub {
        plan tests => scalar keys %examples;
        for ( keys %examples ) {
            reset_mpp();
            warning_like { mpp( $examples{$_}[0] ) } $examples{$_}[1], $_;
        }
    };
}
